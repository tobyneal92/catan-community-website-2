import { createApp, h } from 'vue';
import App from './App.vue'
import VueMarkdownIt from 'vue3-markdown-it';
import router from './router'

createApp({
  render: () => h(App),

}).use(router).use(VueMarkdownIt).mount('#app')
