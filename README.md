# Seasons and league Points

In this community you can earn League Points by competing in the open tournaments and playing matches against each other
on any day of the week! Soon you will also be able to wager the points against each other to climb your way to the top.
The members with the most amount of League Points will be on the leaderboards posted in this channel. Every 3 months the
league will be reset to give everyone a fair chance to compete. The highest ranked members will be eligible for prices
either directly or by getting qualified for an exclusive tournament. The exact information regarding this is yet to be
decided, feel free to leave
your [#💡suggestions](https://discordapp.com/channels/747212662483583069/749987772835823697) .

You can view the current standing at [leaderboard](https://catan-community-website-2.vercel.app/) or by typing `!leaderboard`

You can view the prizes given away in the season 1 finale on the discord under
[#annoucements](https://discordapp.com/channels/747212662483583069/792421487335833601/792428049584488499), close to
400\$ was given away!

We can already guarantee a custom catan board worth 150 $ for season 2! More is depending on
[#💲donations](https://discordapp.com/channels/747212662483583069/749987688387575910/791512565364424715) as we are non
profit!

# Leage Points from the match maker

One way of earning league points is by playing games
in [#colonist](https://discord.com/channels/747212662483583069/750020645185781801) or
in [#catan-universe](https://discord.com/channels/747212662483583069/750012436446314608). Every game is worth 25 LP per
player plus any amount wagered. You can consult this table for a quick reference:


| Players | Wager | Winner gets | Loser loses |
| :------ | :---- | :---------- | :---------- |
| 4 | 0 | 100 | 0 |
| 2 | 0 | 50 | 0 |
|8 | 0 | 200 | 0 |
| 4 | 25 | 175 | 25 |
| 4 | 250 | 850 | 250 |
| $n$ | $m$ |  $n \cdot 25 + (n-1) * m$ | $m$ |

Playing 0 LP games is always 100% safe, as in you don't lose anything if you lose.

# League points during open tournaments

During open tournaments, league points are calculated differently. Every victory point you earn during the tournament
is with 10 LP. Additionally, you get 100 LP extra for every win. If you win with 10 points thus you will earn
200 LP total.

There are additional bonus points for finals:

| Type of game | Points |
| :----- | :----- |
| Day Final | 5x |
| 1-16th place final | 4x |
| 1-16th place losers final | 3x |
| 17-32nd place final | 3x |
| 17-32nd place losers final | 2x |
| 33-48th place final | 2x |
| 33-48th place losers final | 1x |
| 49-64th place | 1x |

Some examples are in the following table:

| Game type | Points | Win | LP |
| :-------- | :----- | :---- | :--- |
| Qualifier | 7 | No | 70 |
| Qualifier | 10 | Yes | 200 |
| Qualifier | 11 | Yes | 210 |
| 1-16th Final | 3 | No | 120 |
| 17-32nd losers final | 10 | Yes | 400 |

# Cash Tournaments

Cash tournaments are not worth any league points, in order to keep the playing field level. Your prize is
already cash anyways, which is (suprisingly) generally considered more valuable than numbers on a screen.