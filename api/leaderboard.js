const mongo = require('mongodb');

module.exports = async (req, res) => {
    let url = "mongodb+srv://" + encodeURIComponent(process.env["MONGODB_USER"]) +
        ":" + encodeURIComponent(process.env.MONGODB_PASSWORD) +
        "@ccdevcluster0.ndot3.mongodb.net/" + encodeURIComponent(process.env["MONGODB_DATABASE"]) + "?retryWrites=true&w=majority";

    let conn = new mongo.MongoClient(
        url
    );

    await conn.connect();

    const database = conn.db(process.env["MONGODB_DATABASE"]);
    res.setHeader(
        'cache-control', 'public, max-age=3600, max-stale=7200' //One hour
    )
    res.json({
        users: (
            await database.collection('users').find({}, {
                "lp": 1,
                "matches": 1,
                "display_name": 1,
                "normalized_wins": 1,
                "tournaments": 1
            }).sort({'lp': -1}).toArray()
        ).map(
            (i) => ({
                "lp": i.lp,
                "matches": i.matches,
                "display_name": i.display_name,
                "winrate": i.normalized_wins / i.matches,
                "tournaments": i.tournaments
            })
        )
    })
}
